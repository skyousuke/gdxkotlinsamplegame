package com.skyousuke.kotlinsamplegame.kotlin

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.utils.TimeUtils

class GameScreenKotlin(val game: DropKotlin) : Screen {

    // load the images for the droplet and the bucket, 64x64 pixels each
    val dropImage = Texture(Gdx.files.internal("droplet.png"))
    val bucketImage = Texture(Gdx.files.internal("bucket.png"))

    // load the drop sound effect and the rain background "music"
    val dropSound = Gdx.audio.newSound(Gdx.files.internal("drop.wav"))
    val rainMusic = Gdx.audio.newMusic(Gdx.files.internal("rain.mp3"))

    // create the camera and the SpriteBatch
    val camera = OrthographicCamera()

    // create a Rectangle to logically represent the bucket
    val bucket = Rectangle()

    // create the raindrops array and spawn the first raindrop
    val raindrops = com.badlogic.gdx.utils.Array<Rectangle>()

    var lastDropTime = 0L
    var dropsGathered = 0

    init {
        camera.setToOrtho(false, 800f, 480f)
        rainMusic.isLooping = true

        bucket.apply {
            x = 800 / 2 - 64 / 2.toFloat() // center the bucket horizontally
            y = 20f // bottom left corner of the bucket is 20 pixels above
            // the bottom screen edge
            width = 64f
            height = 64f
        }

        spawnRaindrop()
    }

    private fun spawnRaindrop() {
        val raindrop = Rectangle()
        raindrop.apply {
            x = MathUtils.random(0, 800 - 64).toFloat()
            y = 480f
            width = 64f
            height = 64f
        }
        raindrops.add(raindrop)
        lastDropTime = TimeUtils.nanoTime()
    }

    override fun render(delta: Float) {
        // clear the screen with a dark blue color. The
        // arguments to glClearColor are the red, green
        // blue and alpha component in the range [0,1]
        // of the color to be used to clear the screen.
        Gdx.gl.glClearColor(0f, 0f, 0.2f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        // tell the camera to update its matrices.
        camera.update()

        // tell the SpriteBatch to render in the
        // coordinate system specified by the camera.
        game.batch.projectionMatrix = camera.combined

        // begin a new batch and draw the bucket and
        // all drops
        game.batch.begin()
        game.font.draw(game.batch, "Drops Collected: $dropsGathered", 0f, 480f)
        game.batch.draw(bucketImage, bucket.x, bucket.y, bucket.width, bucket.height)
        raindrops.forEach {
            game.batch.draw(dropImage, it.x, it.y)
        }
        game.batch.end()

        // process user input
        if (Gdx.input.isTouched) {
            val touchPos = Vector3()
            touchPos.set(Gdx.input.x.toFloat(), Gdx.input.y.toFloat(), 0f)
            camera.unproject(touchPos)
            bucket.x = touchPos.x - 64 / 2
        }

        if (Gdx.input.isKeyPressed(Input.Keys.LEFT))
            bucket.x -= 200 * Gdx.graphics.deltaTime
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT))
            bucket.x += 200 * Gdx.graphics.deltaTime

        // make sure the bucket stays within the screen bounds
        if (bucket.x < 0)
            bucket.x = 0f
        if (bucket.x > 800 - 64)
            bucket.x = 800 - 64.toFloat()

        // check if we need to create a new raindrop
        if (TimeUtils.nanoTime() - lastDropTime > 1000000000)
            spawnRaindrop()

        // move the raindrops, remove any that are beneath the bottom edge of
        // the screen or that hit the bucket. In the later case we increase the
        // value our drops counter and add a sound effect.
        raindrops.forEach{
            it.y -= 200 * Gdx.graphics.deltaTime
            if (it.y + 64 < 0)
                raindrops.removeValue(it, true)
            if (it.overlaps(bucket)) {
                dropsGathered++
                dropSound.play()
                raindrops.removeValue(it, true)
            }
        }
    }

    override fun hide() {}

    override fun show() {
        // start the playback of the background music
        // when the screen is shown
        rainMusic.play()
    }

    override fun pause() {}

    override fun resume() {}

    override fun resize(width: Int, height: Int) {}

    override fun dispose() {
        dropImage.dispose()
        bucketImage.dispose()
        dropSound.dispose()
        rainMusic.dispose()
    }
}