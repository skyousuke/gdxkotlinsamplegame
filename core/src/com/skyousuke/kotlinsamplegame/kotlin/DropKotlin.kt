package com.skyousuke.kotlinsamplegame.kotlin

import com.badlogic.gdx.Game
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch

class DropKotlin : Game() {

    lateinit var batch: SpriteBatch
    lateinit var font: BitmapFont

    override fun create() {
        batch = SpriteBatch()
        //Use LibGDX's default Arial font.
        font = BitmapFont()
        setScreen(MainMenuScreenKotlin(this))
    }

    override fun dispose() {
        batch.dispose()
        font.dispose()
    }
}