package com.skyousuke.kotlinsamplegame.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.skyousuke.kotlinsamplegame.kotlin.DropKotlin

fun main() {
    val config = LwjglApplicationConfiguration()
    config.width = 800
    config.height = 480
    LwjglApplication(DropKotlin(), config)
}